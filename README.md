# Google Drive Demo

This project is a demostration of how connect Wordpress with google api for retreving Files/folders.

## Starting Point

- Clone the project. 
- Find SqlBackup folder.
- Create a db. Database Name: 'web_task_db'.
- Run the script inside SQLBackup Folder into web_task_db database.

## Getting Started

 Make sure GDrive plugin is activated.


## Settings

- Go to Google Drive Settings Demo.
- Configure Google App Client ID, Google App Secret Id, Google App Redirect URI of the application created in Google Developer Console and save them.
- Login to Google. 
- Select a folder from the folder tree wich display your google drive's folders.
 
## Running the tests

- Go to the frontend and you'll view a list of Files/Folders beloging to the selected Folder. You could download files.

  Note: Folders are not downloable.


