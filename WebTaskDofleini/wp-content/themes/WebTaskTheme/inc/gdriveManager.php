<?php
session_start();
require_once (__DIR__.'\vendor\autoload.php');
$client=new Google_Client();

function initClient($clientId,$secretId,$redirectUri,$access_token)
{
    $GLOBALS['client']->addScope(Google_Service_Drive::DRIVE);
    $GLOBALS['client']->setClientId($clientId);
    $GLOBALS['client']->setClientSecret($secretId);
    $GLOBALS['client']->setRedirectUri($redirectUri);
    $GLOBALS['client']->setAccessToken($access_token);
}

function SetAccessToken($code)
{
    if(isset($code))
    {
        $GLOBALS['client']->authenticate($code);
        $_SESSION["access_token"]=$GLOBALS['client']->getAccessToken();
    }
}

function RefreshTokenA($access_token)
{
    $GLOBALS['client']->setAccessToken($access_token);
    if ($GLOBALS['client']->isAccessTokenExpired()) {
        $GLOBALS['client']->fetchAccessTokenWithRefreshToken($GLOBALS['client']->getRefreshToken());
        $_SESSION["access_token"]=$GLOBALS['client']->getAccesstoken();
    }
}

function GetAllFilesWithParents($elemnts,$parent)
{
    $q="mimeType='application/vnd.google-apps.folder' and '".$parent."' in parents and trashed=false";
    $optParams = array(
        'fields' => 'nextPageToken, files(id, name)',
        'q'=>$q
    );
    return  $GLOBALS['$service']->files->listFiles($optParams);
}
?>