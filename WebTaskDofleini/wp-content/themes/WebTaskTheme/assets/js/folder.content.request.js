$(function(){
        var data={action:'folder_content_list'};
    $.ajax({
      url:'wp-admin/admin-ajax.php',
      type:"POST",
      data:data,
      dataType:"json",
      success: function(response){
        debugger;
        $("main").html('<table class="table table-hover">'+
        '<thead>'+
          '<tr class="table-active">'+
          '<th scope="col">Folder/File</th>'+
          '<th scope="col">File Type</th>'+
          '<th scope="col"></th>'+
          '</tr>'+
        '</thead>'+
        '<tbody>');
        var rarray=JSON.parse(JSON.stringify(response.data));
        if(rarray.length==0)
        {
          $("tbody").append("<p>There are not files in the folder selected.</p>")
        }
        else{
        rarray.forEach(element => {
           if(element['webContentLink'])
           {
           $("tbody").append(
               '<tr >'+
               '<td>'+element['text']+'</td>'+
               '<td>'+element['mimeType']+'</td>'+
               '<td>'+'<a href="'+element['webContentLink']+'">Download</a></td></tr>');
           }
           else{
            $("tbody").append(
              '<tr>'+
              '<td>'+element['text']+'</td>'+
              '<td>'+element['mimeType']+'</td>'+
              '<td>No Download</td>'+
              '</tr>'
              );
            }
         }); 


        $("main").append('</tbody></table>')
        }
      },
      error:function(response,ajaxOptions, thrownError){
         debugger;
         $('main').html('<div class="alert alert-danger" role="alert"> <i class="fa fa-exclamation-triangle fa-2x"></i> '+
         '<h4 class="alert-heading">Request Error:</h4>'+
         '<p class="mb-0">'+thrownError+'.</p>'+
         '<hr>'+
         '<ul>'+
          '<li>Check if you are configured google settings in the plugin. Check if you are logged.</li>'+
          '<li>Check if you have internet access.</li>'+
          
         '</ul>'+
         '</div>')
        // $('#treeView').html(response.responseText);
      }
    });


});	

