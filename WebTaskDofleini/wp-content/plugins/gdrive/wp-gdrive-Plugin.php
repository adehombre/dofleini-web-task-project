<?php
/*
 * Plugin Name: Gdrive Demo 
 * Version: 1.0.0 
 * Description: A Google Drive demo plugin
 * Author: Alejandro de Hombre Madrigal<adehombre@gmail.com>
 * Author URI: 
 * Requires at least: 3.6
 * Tested up to: 5.1.1 
 * 
 * @since 1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit;
require_once 'lib/inc/gdriveManager.php';

class oAuth_Demo { 
  private $dir;
  private $file;
  private $token;
  public function __construct( $file ) {
    $this->dir = dirname( $file );
    $this->file = $file;
    $this->token = 'foo_oauth_demo';
    // Register plugin settings
    add_action( 'admin_init' , array( $this , 'register_settings' ) );
    // Add settings page to menu
    add_action( 'admin_menu' , array( $this , 'add_menu_item' ) );
    // Add settings link to plugins page
    add_filter( 'plugin_action_links_' . plugin_basename( $this->file ) , array( $this , 'add_settings_link' ) );
    // setup meta boxes
    // add_action( 'add_meta_boxes', array( $this, 'meta_box_setup' ), 20 );
    // add_action( 'save_post', array( $this, 'meta_box_save' ) ); 
    // NEW: setup the wp ajax action for oAuth code exchange
    add_action( 'wp_ajax_finish_code_exchange', array($this, 'finish_code_exchange') );
    add_action( 'wp_ajax_nopriv_finish_code_exchange', array($this, 'finish_code_exchange') );

    add_action( 'wp_ajax_folder_list', array($this, 'folder_list') );
    add_action( 'wp_ajax_nopriv_folder_list', array($this, 'folder_list') );

    add_action( 'wp_ajax_select_folder', array($this, 'select_folder') );
    add_action( 'wp_ajax_nopriv_select_folder', array($this, 'select_folder') );
    // NEW: setup the wp ajax action to logout from oAuth
    add_action( 'wp_ajax_foo_logout_from_google', array($this, 'logout_from_google') );
    
  }
  
  /* The next few functions set up the settings page */
    public function add_menu_item() {
    add_options_page( 'Google Drive Settings' , 'Google Drive Settings Demo' , 'manage_options' , 'oauth_demo_settings' ,  array( $this , 'settings_page' ) );
  }
  public function add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=oauth_demo_settings">Settings</a>';
    array_push( $links, $settings_link );
    return $links;
  }

  public function register_settings() {
    register_setting( 'oauth_demo_group', 'oauth_demo_settings' );
    add_settings_section('settingssection', 'Google App Settings', array( $this, 'settings_section_callback'), 'oauth_demo_settings');
    //setting fields Definitions
    add_settings_field( 'google_app_client_id', 'Google App Client ID', array( $this, 'settings_field'), 'oauth_demo_settings', 'settingssection', array('setting' => 'oauth_demo_settings', 'field' => 'google_app_client_id', 'label' => '', 'class' => 'regular-text') );
    add_settings_field( 'google_app_client_secret', 'Google App Client Secret', array( $this, 'settings_field'), 'oauth_demo_settings', 'settingssection', array('setting' => 'oauth_demo_settings', 'field' => 'google_app_client_secret', 'label' => '', 'class' => 'regular-text') );
    add_settings_field( 'google_app_redirect_uri', 'Google App Redirect URI', array( $this, 'settings_field'), 'oauth_demo_settings', 'settingssection', array('setting' => 'oauth_demo_settings', 'field' => 'google_app_redirect_uri', 'label' => '', 'class' => 'regular-text') );
  }
  public function settings_section_callback() { echo ' '; }
  public function settings_field( $args ) {
    // Creating text input fields.  Because it accepts a class, it can be styled. Pass in a 'default' argument only if you want a non-empty default value.
    $settingname = esc_attr( $args['setting'] );
    $setting = get_option($settingname);
    $field = esc_attr( $args['field'] );
    $label = esc_attr( $args['label'] );
    $class = esc_attr( $args['class'] );
    $default = ($args['default'] ? esc_attr( $args['default'] ) : '' );
    $value = (($setting[$field] && strlen(trim($setting[$field]))) ? $setting[$field] : $default);
    echo '<input type="text" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" class="' . $class . '" value="' . $value . '" /><p class="description">' . $label . '</p>';
  }

  //Building form setting using setting fields
  public function settings_page() {
    if (!current_user_can('manage_options')) {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }
    ?>
    <div class="wrap">
      <h2>GDrive Demo Settings</h2>
      <p>You'll need to go to the <a href="https://console.developers.google.com">Google Developer Console</a> to setup your project and setup the values below.</p>
      <form action="options.php" method="POST">
        <?php settings_fields( 'oauth_demo_group' ); ?>
        <?php do_settings_sections( 'oauth_demo_settings' ); ?>
        <?php submit_button(); ?>
      </form>
    <!-- Handling the login process on the settings page now -->
    <?php $this->write_out_oAuth_JavaScript(); ?>

    <h5 id="mappedFiles"></h5>
    <div id="treeView">
    </div>
    </div>
    <?php
  }
  // This function is the clearest way to get the oAuth JavaScript onto a page as needed.
  private function write_out_oAuth_JavaScript() {
    $settings = get_option('oauth_demo_settings', true);
    ?>
  <script language=javascript>

    // we declare this variable at the top level scope to make it easier to pass around
  var google_access_token = "<?php echo $this->get_google_access_token(); ?>";
  jQuery(document).ready(function($) {
    var GOOGLECLIENTID = "<?php echo $settings['google_app_client_id']; ?>";
    var GOOGLECLIENTREDIRECT = "<?php echo $settings['google_app_redirect_uri']; ?>";
    // we don't need the client secret for this, and should not expose it to the web.
  function requestGoogleoAuthCode() {
    var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth';
    var SCOPE = 'profile email openid https://www.googleapis.com/auth/drive';
    var popupurl = OAUTHURL + '?scope=' + SCOPE + '&client_id=' + GOOGLECLIENTID + '&redirect_uri=' + GOOGLECLIENTREDIRECT + '&response_type=code&access_type=offline&prompt=select_account consent';
    var win =   window.open(popupurl, "googleauthwindow", 'width=800, height=600'); 
    var pollTimer = window.setInterval(function() { 
      try {
        if (win.document.URL.indexOf(GOOGLECLIENTREDIRECT) != -1) {
          window.clearInterval(pollTimer);
          var response_url = win.document.URL;
          var auth_code = gup(response_url, 'code');
          win.close();
          // We don't have an access token yet, have to go to the server for it
          var data = {
            action: 'finish_code_exchange',
            auth_code: auth_code
          };
          $.post(ajaxurl, data, function(response) {       
            google_access_token = response;
            getGoogleUserInfo(google_access_token);
            
          }).done(function() 
          {
            // Building Hierarchical Tree Folders
            var data={action:'folder_list'};
            $.ajax({
              url:ajaxurl,
              type:"POST",
              data:data,
              dataType:"json",
              success: function(response){
                // $('#treeView').jstree(true).settings.core.data = response;
                // $('#treeView').jstree(true).redraw(true);
                 $('#treeView').jstree({ 'core' : {
                  'data' : response
                 } });
                
              },
              error:function(response,ajaxOptions, thrownError){
                    $('#treeView').html("<span>Error: "+ thrownError+"</span>");
              }
            });
         });
        }
      } catch(e) {}    
    }, 500);
  }

  // helper function to parse out the query string params
  function gup(url, name) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?#&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    if( results == null )
      return "";
    else
    {
      return results[1];
    }
      
  }
  function getGoogleUserInfo(google_access_token) {
    
    $.ajax({
      url: 'https://www.googleapis.com/plus/v1/people/me/openIdConnect',
      data: {
        access_token: google_access_token
      },
      success: function(resp) {
        
        var user = resp;
        $('#googleUserName').text('You are logged in as ' + user.name);
        loggedInToGoogle = true;
        $('#google-login-block').hide();
        $('#google-logout-block').show();
      },
      dataType: "jsonp"
    });
  }
  function logoutFromGoogle() {
    $.ajax({
      url: ajaxurl,
      data: {
        action: 'foo_logout_from_google'
      },
      success: function(resp) {
        $('#googleUserName').text(resp);
        $('#google-login-block').show();
        $('#google-logout-block').hide();
        google_access_token = '';
        //$("#treeView").html('');
      }
    });
  }
  // We also want to setup the initial click event and page status on document.ready
   $(function() {
    $('#google-login-block').click(requestGoogleoAuthCode);
    $('#google-logout-block').hide();
    $('#google-logout-block').click(logoutFromGoogle);
    // now lets show that they're logged in if they are
    if (google_access_token) {
      getGoogleUserInfo(google_access_token);
    }
   });  
  });
  </script>
  <a id="google-login-block">Login to Google </a>
  <span id="googleUserName">You are not logged in </span>
  <span id="google-logout-block"><a>Logout from Google</a></span>
  <iframe id="googleAuthIFrame" style="visibility:hidden;" width=1 height=1></iframe>
  <?php
  // END inlined JavaScript and HTML
  }

  
  
  /* NEW section to handle doing oAuth server-to-server */
  // wrapper for wp_ajax to point to reusable function
  public function finish_code_exchange() {
    $auth_code = ( isset( $_POST['auth_code'] ) ) ? $_POST['auth_code'] : '';
    //echo $auth_code;
    echo $this->set_google_oauth2_token($auth_code, 'auth_code');
    wp_die(); 
  }


public function folder_list()
{
  $settings = get_option('oauth_demo_settings', true);

  $googleClient=initGoogleDriveClient(
    $settins['google_app_client_id'],
    $settins['google_app_client_secret'],
    $settins['google_app_redirect_uri'],
    get_option('foo_google_access_token'));

     echo GetAllFolders();
     wp_die();
}

public function select_folder()
{
    update_option('google_selected_folder_id',$_POST['IdFolderSelected'],'',true);
    echo $_POST['NodeTextFolderSelected'];
    wp_die();
}

private function set_google_oauth2_token($grantCode, $grantType) {
   /* based on code written by Jennifer L Kang that I found here
   * http://www.jensbits.com/2012/01/09/google-api-offline-access-using-oauth-2-0-refresh-token/
   * and modified to integrate with WordPress and to calculate and store the expiration date.
    */  
    $settings = get_option('oauth_demo_settings', true);
    //var_dump($settings);
    $success = true;	
    $oauth2token_url = "https://accounts.google.com/o/oauth2/token";
    $clienttoken_post = array(
      "client_id" => $settings['google_app_client_id'],
      "client_secret" => $settings['google_app_client_secret']
    );
    if ($grantType === "auth_code"){
      $clienttoken_post["code"] = $grantCode;	
      $clienttoken_post["redirect_uri"] = $settings['google_app_redirect_uri'];
      $clienttoken_post["grant_type"] = "authorization_code";
    }
    if ($grantType === "refresh_token"){
      $clienttoken_post["refresh_token"] = get_option('foo_google_refresh_token', true);
      $clienttoken_post["grant_type"] = "refresh_token";
    }
    $postargs = array(
      'body' => $clienttoken_post
     );
    $response = wp_remote_post($oauth2token_url, $postargs );
    $authObj = json_decode(wp_remote_retrieve_body( $response ), true);
    if (isset($authObj['refresh_token'])){
      $refreshToken = $authObj['refresh_token'];
      $success = update_option('foo_google_refresh_token', $refreshToken, false); 
      // the final 'false' is so we don't autoload this value into memory on every page load
    }
    if ($success) {
      $success = update_option('foo_google_access_token_expires',  strtotime("+" . $authObj['expires_in'] . " seconds"));
    }
    if ($success) {
      $success = update_option('foo_google_access_token', $authObj[access_token], false);
      if ($success) {
        $success = $authObj[access_token];
      }
    }
    // if there were any errors $success will be false, otherwise it'll be the access token
    if (!$success) { $success=false; }
    return $success;
  }
  public function get_google_access_token() {
    $expiration_time = get_option('foo_google_access_token_expires', true);
    if (! $expiration_time) {
      return false;
    }
    // Give the access token a 5 minute buffer (300 seconds)
    $expiration_time = $expiration_time - 300;
    if (time() < $expiration_time) {
      return get_option('foo_google_access_token', true);
    }
    // at this point we have an expiration time but it is in the past or will be very soon
    return $this->set_google_oauth2_token(null, 'refresh_token');
  }
  public function revoke_google_tokens() {
    /* This function finds either the access token or refresh token
     * revokes them with google (revoking the access token does the refresh too)
     * then deletes the data from the options table
    */
    $return = '';
    $token = get_option('foo_google_access_token', true);
    $expiration_time = get_option('foo_google_access_token_expires', true);
    if (!$token || (time() > $expiration_time)){
      $token = get_option('foo_google_refresh_token', true);
    }
    if ($token) {
      $return = wp_remote_retrieve_response_code(wp_remote_get("https://accounts.google.com/o/oauth2/revoke?token=" . $token));
    } else {
      $return = "no tokens found";
    }
    if ($return == 200) {
      delete_option('foo_google_access_token');
      delete_option('foo_google_refresh_token');
      delete_option('foo_google_access_token_expires');
      return true;
    } else {
      return $return; 
    }
  }
  // wrapper for wp_ajax to point to reusable function
  public function logout_from_google() {
    $response = $this->revoke_google_tokens();
    if ($response === true) {
      $response = "success";
    }
    echo $response;
    wp_die(); 
  }
 
 
 
  private function format_input_field_as_tablerow($option, $field, $value) {
    $html = '';
    if ($option['maxlength']) { $maxinput = ' data-limit-input="' . $option['maxlength'] . '" '; }
    $html .= '<tr valign="top" class="' . $option['section'] . '"><th scope="row"><label for="' . esc_attr( $field ) . '">' . $option['name'] . '</label></th><td><input name="' . esc_attr( $field ) . '" type="text" id="' . esc_attr( $field ) . '" class="regular-text" value="' . esc_attr( $value ) . '"' . $maxinput . ' />' . "\n";
    $html .= '<span></span><p class="description">' . $option['description'] . '</p>' . "\n";
    $html .= '</td></tr>' . "\n";
    return $html;
  }
  
//end of class  
}
// Instantiate our class
add_action( 'admin_enqueue_scripts', 'include_extra_scripts' );
function include_extra_scripts() {
  wp_enqueue_script( 'jqueryscript','https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js');
  wp_enqueue_script( 'jstree_js', plugin_dir_url( __FILE__ ) . '/jstree/jstree.min.js');

  wp_register_style( 'jstree_css',plugin_dir_url( __FILE__ ) . '/jstree/themes/default/style.min.css', false, '1.0.0' );
  wp_enqueue_style( 'jstree_css' );

  wp_enqueue_script( 'node_tree_selected_js', plugin_dir_url( __FILE__ ) . '/node_tree_changed.js');
  
}


global $plugin_obj;
$plugin_obj = new oAuth_Demo( __FILE__ );

// always cleanup after yourself
register_deactivation_hook(__FILE__, 'foo_deactivation');
function foo_deactivation() {
  // delete the google tokens
  $plugin_obj = new oAuth_Demo( __FILE__ );
  $plugin_obj->revoke_google_tokens();
  delete_option('oauth_demo_settings');

  error_log('Foo has been deactivated');
}
/* END OF FILE */
?>