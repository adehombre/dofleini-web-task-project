<?php
session_start();
require_once (__DIR__.'\vendor\autoload.php');

$client=new Google_Client();


function initGoogleDriveClient($clientId,$secretId,$redirectUri,$access_token)
{
    $GLOBALS['client']->addScope(Google_Service_Drive::DRIVE);
    $GLOBALS['client']->setClientId($clientId);
    $GLOBALS['client']->setClientSecret($secretId);
    $GLOBALS['client']->setRedirectUri($redirectUri);
    //RefreshToken($access_token);
    $GLOBALS['client']->setAccessToken($access_token);
}

function getServiceGoogleDrive($client)
{
  return new Google_Service_Drive($client);
}

function AuthenticateWithCode_SetAccessToken($code)
{
    if(isset($code))
    {
        $GLOBALS['client']->authenticate($code);
        $_SESSION["access_token"]=$GLOBALS['client']->getAccessToken();
    }
}

function RefreshToken($access_token)
{
    $GLOBALS['client']->setAccessToken($access_token);
    if ($GLOBALS['client']->isAccessTokenExpired()) {
        $GLOBALS['client']->fetchAccessTokenWithRefreshToken($GLOBALS['client']->getRefreshToken());
        $_SESSION["access_token"]=$GLOBALS['client']->getAccesstoken();
    }
}


function PrintFoldersHierarchical($service, $folderId)
{
    return $folders;    
}

 function GetAllFolders()
{
    //QUERY FOR GETTING ONLY FOLDERS NOT TRASHED
    $q="mimeType='application/vnd.google-apps.folder' and trashed=false";

    $optParams = array(
        'fields' => 'nextPageToken, files(id, name,parents)',
        'q'=>$q
    );
    $result=array();
    $service=new Google_Service_Drive($GLOBALS['client']);
    $folderList=$service->files->listFiles($optParams);
    //Building up array with required field that jstree needs
    foreach ($folderList as $key => $value)
    {
      //By default $padre is #
      $padre="#";
      //Get rootId 
      $root=$service->files->get('root');
      $rootId=$root->getId();
      //Set $padre the last parent
      foreach ($value->parents as $item)
        $padre=$item;

      if($padre==$rootId)
        $padre="#";
      array_push($result, ["id"=>$value->id,"parent"=>$padre,"text"=>$value->name]);  
    }
    return json_encode($result);
}

function GetAllFoldersWithParents($parentId)
{
    $q="'".$parentId."' in parents and trashed=false";
    //$q="mimeType='application/vnd.google-apps.folder' and trashed=false";
    $optParams = array(
        'fields' => 'nextPageToken, files(id, name,webContentLink,mimeType,parents)',
        'q'=>$q
    );
     $result=array();
     $service=new Google_Service_Drive($GLOBALS['client']);
     $folderList=$service->files->listFiles($optParams);
    
    foreach ($folderList as $key => $value)
    {
        array_push($result, ["id"=>$value->id,"text"=>$value->name,"webContentLink"=>$value->webContentLink,"mimeType"=>$value->mimeType]);  
    }
    return json_encode(["data"=>$result]);
}

function buildTree(array &$elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[$element['id']] = $element;
            unset($elements[$element['id']]);
        }
    }
    return $branch;
}


function printFilesInFolder($service, $folderId) {
    $pageToken = NULL;
   
    do {
      try {
        $parameters = array();
        if ($pageToken) {
          $parameters['pageToken'] = $pageToken;
        }
        
        $parameters['fields']= 'nextPageToken, files(id, name,mimeType,kind,fileExtension,webContentLink)';
        $parameters['q']="'".$folderId."'".' in parents';
        
        $children = $service->files->listFiles($parameters);
  
  
        foreach ($children->getFiles() as $child) {
          $urlDownload= $child->getWebContentLink();
              
          echo "<span style='padding-left:30px;display:block'><a href='". $urlDownload."'>". $child->getName()."</a></span>";
          //print 'File Id: ' . $child->getId();
        }
        $pageToken = $children->getNextPageToken();
      } catch (Exception $e) {
        print "An error occurred: " . $e->getMessage();
        $pageToken = NULL;
      }
    } while ($pageToken);
  }

  function retrieveAllFiles($service,$q) {
    $result = array();
    $pageToken = NULL;
  
    do {
      try {
        $parameters = array();
        if ($pageToken) {
          $parameters['pageToken'] = $pageToken;
        }
        $parameters['q']=$q;
        $parameters['fields']='files(id, name,mimeType,kind,fileExtension,webContentLink,parents)';

        $files = $service->files->listFiles($parameters);
        $result = array_merge($result, $files->getFiles());


        $pageToken = $files->getNextPageToken();
      } catch (Exception $e) {
        print "An error occurred: " . $e->getMessage();
        $pageToken = NULL;
      }
    } while ($pageToken);
    return $result;
  }
?>