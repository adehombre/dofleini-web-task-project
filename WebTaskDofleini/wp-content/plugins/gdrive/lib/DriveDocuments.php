<?php
session_start();
require_once 'lib/inc/vendor/autoload.php';
$client=new Google_Client();
$client->addScope(Google_Service_Drive::DRIVE);

$access_token=$_SESSION["access_token"];
if (isset($access_token))
{
    $client->setAccessToken($access_token);
    try {
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        $_SESSION["access_token"]=$client->getAccesstoken();
    }

    $listFoldersQuery="mimeType='application/vnd.google-apps.folder' and 'root' in parents and trashed=false";
  
    $service = new Google_Service_Drive($client);
    $optParams = array(
        'q' => $listFoldersQuery,
        'fields' => 'nextPageToken, files(id, name,mimeType,kind,fileExtension,webContentLink)',
        'orderBy'=>array('name')
    );

    $results = $service->files->listFiles($optParams);
    //$service=new Google_Service_Drive($client);
    if (count($results->getFiles()) == 0) {
        echo "No files found.\n";
    } else {
        print "Files:\n";
        ?>
        <ul>
        <?php
        //Printing Folders
      
        //  $json= json_encode($results->getFiles());
        //  var_dump($json);
        foreach ($results->getFiles() as $file) {
           
            $urlDownload= $file->getWebContentLink();
            echo "<li><a href='". $urlDownload."'>". $file->getName()."</a></li>";
            printFilesInFolder($service,$file->getId());
        }
        
        ?>
</ul>
    <?php }
}
catch(Exception $e)
{
    echo "<b>".$e->getMessage()."</b>";
}
}


function printFilesInFolder($service, $folderId) {
    
  $pageToken = NULL;

  do {
    try {
      $parameters = array();
      if ($pageToken) {
        $parameters['pageToken'] = $pageToken;
      }
      
      $parameters['fields']= 'nextPageToken, files(id, name,mimeType,kind,fileExtension,webContentLink)';
      $parameters['q']="'".$folderId."'".' in parents';
      
      $children = $service->files->listFiles($parameters);


      foreach ($children->getFiles() as $child) {
        $urlDownload= $child->getWebContentLink();
            
        echo "<span style='padding-left:30px;display:block'><a href='". $urlDownload."'>". $child->getName()."</a></span>";
        //print 'File Id: ' . $child->getId();
      }
      $pageToken = $children->getNextPageToken();
    } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
      $pageToken = NULL;
    }
  } while ($pageToken);
}

?>



