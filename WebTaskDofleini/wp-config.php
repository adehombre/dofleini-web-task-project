<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'web_task_db' );

/** MySQL database username */
define( 'DB_USER', 'alejandro' );

/** MySQL database password */
define( 'DB_PASSWORD', 'alejandro' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<BNkDCC]Gl>.KV@m_))fO9I:ptjb:C0=5 %^RW26]_SqC,RxHah|HN*%N1WfbW}e' );
define( 'SECURE_AUTH_KEY',  '_Iy@@%B@_96,G%hTSwR|M&8qpJ6WQZ/^`mY0I@dg*Xi+yoKMg&?kaNJi,Fi2qHxu' );
define( 'LOGGED_IN_KEY',    'bgMJw5F{yU!iV%87%]%]9SBBw1G_[D%?m~IwF~,6Hq$=*`I}?SniP?k=MB&:^*R]' );
define( 'NONCE_KEY',        'Mw%_Zf>3J,:,9=#c%D%t4Qb@o@rjk8oLSY|={=f,L:p&}~+tO<_/Ohb^y^ft~/a[' );
define( 'AUTH_SALT',        'm#$<KJhF=d3.=5Eq7f&2AE|bswZm5(x>5vwMRYA{-[MI+l>$+G<vs{n:(-]#+;Ze' );
define( 'SECURE_AUTH_SALT', 'rD-)^gKuLs>wEb}SKRUTRP.NWX)_/+M_X&t><zF<*ogKJ6jL3oH5,Vk^,%sggSOg' );
define( 'LOGGED_IN_SALT',   '>3g!=6aU64gR<*qm{t|;/@1c_|N4Q(K}h2tN*K*=%w5~U9[3NM25Ry/4O[=s>h-B' );
define( 'NONCE_SALT',       'v:jjRsx_4ER3(D[ey+dRJ$lJn)%@}G6z;<c=A1W6Z3`}::dP7,>%3V9rveX:b1kW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

// Turns WordPress debugging on
// define('WP_DEBUG', true);

// // Tells WordPress to log everything to the /wp-content/debug.log file
// define('WP_DEBUG_LOG', true);

// // Doesn't force the PHP 'display_errors' variable to be on
// define('WP_DEBUG_DISPLAY', false);

// // Hides errors from being displayed on-screen
// @ini_set('display_errors', 0);

function log_me($message) {
    if (WP_DEBUG === true) {
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
    }
}
log_me("Testing log");


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
